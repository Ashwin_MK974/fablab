# Projet Fablab - Script d'installation

Création d'un script permet l'installation de divers outils pour le projet Fablab.
Ce script est configuré pour fonctionner sur un environnement Debian.



# Installation 
Le  script permet d'installer le process permettent de gérer l'installation de PowerDNS. Celui-ci est possible en exécutant la commande suivante : 

    apt update -y && apt install wget -y && wget https://gitlab.com/Ashwin_MK974/fablab/-/raw/main/fablab.sh && chmod +x fablab.sh && bash fablab.sh &&  . ~/.bashrc

# Installation  de PowerDNS
Une fois le script installé, il suffit alors d'exécuter la commande suivante pour procéder à l'installation de PowerDNS

    fablab install powerdns

# Lancement du serveur
La commande suivante permet le lancement du serveur PowerDNS. L'adresse IP suivi du port sera ainsi afficher dans le terminal afin de pouvoir se connecter.

    fablab run powerdns
