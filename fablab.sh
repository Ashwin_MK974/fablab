#!/bin/bash

#Exemple de configuration :
DNS_SERVER_IP='34.78.147.233'
DNS_SERVER_NETWORK='127.0.0.1/8'
TEST_SERVER_DESTINATION='8.8.8.8'
SECRET_KEY='e951e5a1f4b94151b360f47edf596dd2'
BIND_ADDRESS='127.0.0.1'
PORT='9191'
DATABASE_NAME='powerdnsfablab'
DATABASE_USER='pdnsadmin'
DATABASE_PASSWORD='powerdnspassword'














function success_write(){
    echo ""
    echo -e "\e[1m$1 ✔️\e[0m"
    echo -e "\e[97m"
}
function fail_write(){
   echo ""
   echo -e "\e[1m$1 ❌\e[0m"
   echo -e "\e[97m"
}
function green_write(){
    echo ""
    echo -e "\e[32m$1"
    echo -e "\e[97m"
}
function green_write_sucess(){
    echo ""
    echo -e "\e[32m$1✔️"
    echo -e "\e[97m"
}

function important_write(){
    echo ""
    echo -e "\e[1m$1\e[0m"
    echo -e "\e[97m"
}

function get_answer() 
{
     echo ""
    echo -e "\e[1m$1 🔍\e[0m"
    echo -e "\e[97m"   
}

function warning_write(){
    echo ""
    echo -e "\e[1m""⚠️ "  $1 "\e[0m"
    echo -e "\e[97m"
}
function italic_write(){
    echo ""
    echo -e "\033[3m$1\033[23m"
    echo -e "\e[97m"
}
function italic_warning_write(){
    echo ""
    echo -e "\033[3m$1 ⚠️\033[23m"
    echo -e "\e[97m"
}
function italic_write_warning(){
    echo ""
    echo -e "\033[3m$1 ⚠️\033[23m"
    echo -e "\e[97m"
}

if grep -Fxq "#fablab:true" ~/.bashrc
then
    echo ""
else
    important_write "Installation des allias"
    echo "#fablab:true">> ~/.bashrc;
    echo "Creation des alias pour Fablab";
    echo "alias edit-a='nano ~/.bashrc'">> ~/.bashrc;
    echo "alias src='source ~/.bashrc'">> ~/.bashrc;
    echo "alias fablab='bash /etc/fablab/./fablab.sh fablab_init'" >> ~/.bashrc;
    echo "#fablab end alias">> ~/.bashrc;
    source ~/.bashrc;
    green_write_sucess "Allias installé avec succès"
fi

if [ ! -d /etc/fablab/ ]
then
    mkdir /etc/fablab/
    cp $PWD/fablab.sh /etc/fablab/fablab.sh
fi



if [ ! -e /etc/fablab/fablab_state ]
then
   echo "Création fichier d'état"
   touch /etc/fablab/fablab_state
fi

function verify_state(){
                    if [ -e /etc/fablab/fablab_state ]
                    then
                                    if grep -Fxq $1 /etc/fablab/fablab_state
                                    then
                                            if [[ $5 == "--v" ]]
                                            then
                                                    green_write_sucess "$4 à déjà été installé"
                                            fi
                                            return $2
                                    else
                                            return $3     
                                    fi
                    fi
}

verify_state "nano_install_true" 1 0 nano && apt install nano -y && echo "nano_install_true" >> /etc/fablab/fablab_state
#echo "powerdns_install_true" >> /etc/fablab/fablab_state
fablab_init () 
{

    important_write "Fablab Program"
    if [[ $1 == "install" && $2 == "powerdns" && $3 == "and" && $4 == "powerdnsAdmin" ]]
    then
        verify_state "powerdns_install_true" 1 0 powerdns --v && verify_state "powerdnsAdmin_install_true" 1 0 powerdnsAdmin --v &&
        if [[ -z $DNS_SERVER_IP || -z $DNS_SERVER_NETWORK || -z $TEST_SERVER_DESTINATION || -z $SECRET_KEY ||  -z $BIND_ADDRESS || -z $PORT || -z $DATABASE_NAME || -z $DATABASE_USER || -z $DATABASE_PASSWORD  ]]
        then
            warning_write "Veuillez renseigner toutes les données de configuration et réasayer"
            italic_write "Ouverture du fichier ..."
            sleep 5
            nano /etc/fablab/fablab.sh
        else
            important_write "Installation de PowerDNS ainsi que PowerDNSAdmin"
            install_powerdns
            powerdnsAdmin
        fi
    elif [[ $1 == "install" && $2 == "powerdns" && $3 == "" && $4 == "" ]]
    then
        verify_state "powerdns_install_true" 1 0 powerdns --v && echo "Installation de PowerDNS" && install_powerdns
    elif [[ $1 == "install" && $2 == "powerdnsAdmin" && $3 == "" && $4 == "" ]] 
    then
         verify_state "powerdnsAdmin_install_true" 1 0 powerdnsAdmin --v && echo "Installation de PowerDNS && PowerDNSAdmin"  && powerdnsAdmin
    elif [[ $1 == "start" && $2 == "powerdns" ]]  
    then
          run_serve       
    elif [[ $1 == "update" && $2 == "powerdns" ]]  
    then
          fablab_update         
    else
          warning_write "Echec : Preciser des argument "
          italic_write "Exemple : fablab install powerdns and powerdnsAdmin"
    fi
          
}


fablab_update(){
echo "Version 1.2"
if [ -e /etc/fablab/fablab.sh ]
then
    rm /etc/fablab/fablab.sh
   (cd /etc/fablab ; wget https://gitlab.com/Ashwin_MK974/fablab/-/raw/main/fablab.sh && chmod +x fablab.sh && bash fablab.sh)
    green_write_sucess "Script Fablab mis à jour"
else
    important_write "Création du répértoire Fablab"
    mkdir /etc/fablab
    (cd /etc/fablab ; wget https://gitlab.com/Ashwin_MK974/fablab/-/raw/main/fablab.sh && chmod +x fablab.sh && bash fablab.sh)
    green_write_sucess "Script Fablab installé"  
fi
}

function install_powerdns()
{
important_write "Installation de PowerDNS"
apt update -y && 
apt install wget -y &&
apt install ufw -y && 
apt install git -y &&
apt install software-properties-common gnupg2 -y && 
sudo apt install mariadb-server -y &&
mysql_secure_installation <<EOF
powerdnspassword
Y
n
n
n
n
n
n
n
EOF
systemctl disable --now systemd-resolved &&
echo "nameserver 8.8.8.8" >> /etc/resolv.conf &&
echo "deb [arch=amd64] http://repo.powerdns.com/debian bullseye-auth-45 main" | sudo tee /etc/apt/sources.list.d/pdns.list &&
cat > /etc/apt/preferences.d/pdns << EOL
Package: pdns-*
Pin: origin repo.powerdns.com
Pin-Priority: 600
EOL
wget -qO- https://repo.powerdns.com/FD380FBB-pub.asc | gpg --dearmor > /etc/apt/trusted.gpg.d/pdns.gpg &&
apt update -y &&
apt install pdns-server -y &&
apt install pdns-backend-mysql -y &&
mysql -u root << EOL
create database $DATABASE_NAME;
grant all on $DATABASE_NAME.* to $DATABASE_USER@localhost identified by $DATABASE_PASSWORD;
flush privileges;
quit
EOL
mysql -u $DATABASE_USER -$DATABASE_PASSWORD  $DATABASE_NAME < /usr/share/pdns-backend-mysql/schema/schema.mysql.sql
mysqlshow $DATABASE_NAME &&
cat > /etc/powerdns/pdns.d/pdns.local.gmysql.conf << 'EOL'
# MySQL Configuration
#
# Launch gmysql backend
launch+=gmysql

# gmysql parameters
gmysql-host=127.0.0.1
gmysql-port=3306
gmysql-dbname=$DATABASE_NAME
gmysql-user=$DATABASE_USER
gmysql-password=$DATABASE_PASSWORD
gmysql-dnssec=yes
# gmysql-socket=
EOL
chown pdns: /etc/powerdns/pdns.d/pdns.local.gmysql.conf &&
chmod 640 /etc/powerdns/pdns.d/pdns.local.gmysql.conf && 
systemctl stop pdns.service &&
systemctl restart pdns &&
ss -alnp4 | grep pdns &&
apt install dnsutils -y &&
important_write "Création d'une zone par défaut"
#italic_write "Entrer l'adresse ip du serveur DNS"
ipdns=$DNS_SERVER_IP
#italic_write "Entrer l'adresse ip du serveur de destination"
ip_dns_dest=$TEST_SERVER_DESTINATION
pdnsutil create-zone test-mks.io && 
pdnsutil add-record test-mks.io ns1 A $ipdns
pdnsutil add-record test-mks.io @ NS ns1.test-mks.io &&
pdnsutil add-record test-mks.io @ A $ip_dns_dest && 
pdnsutil add-record test-mks.io www A $ip_dns_dest &&
pdnsutil list-zone test-mks.io && 
pdnsutil check-all-zones &&
dig test-mks.io @$ipdns && 
pdnsutil list-all-zones native &&
apt install dnsutils -y &&
#italic_write "Entrer l'adresse réseau du serveur dns exemple => 137.184.9.0/24  "
network=$DNS_SERVER_NETWORK
sudo ufw allow from $network to any port 53 proto udp && 
echo "nameserver $ipdns" >> /etc/resolv.conf &&
success_write "Installation de PowerDNS réusie" && 
echo "powerdns_install_true" >> /etc/fablab/fablab_state
}
function powerdnsAdmin()
{
echo "Installation de PowerDnsAdmin"
apt install python3-dev -y &&
apt install  libldap2-dev libssl-dev libxml2-dev libxslt1-dev libxmlsec1-dev libffi-dev pkg-config apt-transport-https virtualenv build-essential libmariadb-dev git python3-flask -y
#apt install libsasl2-3dev
apt install curl sudo git -y && 
#curl -sL https://deb.nodesource.com/setup_17.x | sudo -E bash - && 
apt install -y nodejs && apt-get install npm -y && npm install -g n && n latest && 
curl -sL https://dl.yarnpkg.com/debian/pubkey.gpg | gpg --dearmor | sudo tee /usr/share/keyrings/yarnkey.gpg >/dev/null &&
echo "deb [signed-by=/usr/share/keyrings/yarnkey.gpg] https://dl.yarnpkg.com/debian stable main" | sudo tee /etc/apt/sources.list.d/yarn.list &&
apt update -y && 
apt install yarn -y &&
apt install nginx -y && 
git clone https://github.com/ngoduykhanh/PowerDNS-Admin.git /var/www/html/pdns &&
#error
sudo apt-get update
sudo apt-get install libsasl2-dev
cd /var/www/html/pdns && virtualenv -p python3 flask && source ./flask/bin/activate && pip install -r requirements.txt && deactivate &&

cat > /var/www/html/pdns/powerdnsadmin/default_config.py << 'EOL'
import os
import urllib.parse
basedir = os.path.abspath(os.path.dirname(__file__))

### BASIC APP CONFIG
SALT = '$2b$12$yLUMTIfl21FKJQpTkRQXCu'
SECRET_KEY=$SECRET_KEY
BIND_ADDRESS=$BIND_ADDRESS
PORT=$PORT
HSTS_ENABLED = False
OFFLINE_MODE = False
FILESYSTEM_SESSIONS_ENABLED = False
SESSION_COOKIE_SAMESITE = 'Lax'
CSRF_COOKIE_HTTPONLY = True

### DATABASE CONFIG
SQLA_DB_USER=$DATABASE_USER
SQLA_DB_PASSWORD=$DATABASE_PASSWORD
SQLA_DB_HOST = '127.0.0.1'
SQLA_DB_NAME=$DATABASE_NAME
SQLALCHEMY_TRACK_MODIFICATIONS = True

### DATABASE - MySQL
SQLALCHEMY_DATABASE_URI = 'mysql://{}:{}@{}/{}'.format(
    urllib.parse.quote_plus(SQLA_DB_USER),
    urllib.parse.quote_plus(SQLA_DB_PASSWORD),
    SQLA_DB_HOST,
    SQLA_DB_NAME
)

### DATABASE - SQLite
# SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(basedir, 'pdns.db')

# SAML Authnetication
SAML_ENABLED = False
SAML_ASSERTION_ENCRYPTED = True
EOL

#sudo apt remove yarn -y && curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add - && echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list && sudo apt update -y && sudo apt install --no-install-recommends yarn -y &&
(cd /var/www/html/pdns/ ; source ./flask/bin/activate && export FLASK_APP=powerdnsadmin/__init__.py && flask db upgrade && yarn install --pure-lockfile && flask assets build && deactivate )
cat > /etc/powerdns/pdns.conf << 'EOL'           
#################################
# api   Enable/disable the REST API (including HTTP listener)
#
# api=no
api=yes

#################################
# api-key       Static pre-shared authentication key for access to the REST API
#
# api-key=
api-key=ahqu4eiv2vaideep8AQu9nav5Aing0
EOL
#systemctl restart pdns

cat > /etc/nginx/conf.d/pdns-admin.conf << 'EOL'           
server {
  listen	*:80;
  server_name               pdnsadmin.kifarunix-demo.com;

  index                     index.html index.htm index.php;
  root                      /var/www/html/pdns;
  access_log                /var/log/nginx/pdnsadmin_access.log combined;
  error_log                 /var/log/nginx/pdnsadmin_error.log;

  client_max_body_size              10m;
  client_body_buffer_size           128k;
  proxy_redirect                    off;
  proxy_connect_timeout             90;
  proxy_send_timeout                90;
  proxy_read_timeout                90;
  proxy_buffers                     32 4k;
  proxy_buffer_size                 8k;
  proxy_set_header                  Host $host;
  proxy_set_header                  X-Real-IP $remote_addr;
  proxy_set_header                  X-Forwarded-For $proxy_add_x_forwarded_for;
  proxy_headers_hash_bucket_size    64;

  location ~ ^/static/  {
    include  /etc/nginx/mime.types;
    root /var/www/html/pdns/powerdnsadmin;

    location ~*  \.(jpg|jpeg|png|gif)$ {
      expires 365d;
    }

    location ~* ^.+.(css|js)$ {
      expires 7d;
    }
  }

  location / {
    proxy_pass            http://unix:/run/pdnsadmin/socket;
    proxy_read_timeout    120;
    proxy_connect_timeout 120;
    proxy_redirect        off;
  }

}
EOL
mv /etc/nginx/sites-enabled/default{,.old} &&
sudo  &&
chown -R www-data: /var/www/html/pdns &&
systemctl restart nginx &&
cat > /etc/systemd/system/pdnsadmin.service << 'EOL'           
[Unit]
Description=PowerDNS-Admin
Requires=pdnsadmin.socket
After=network.target

[Service]
PIDFile=/run/pdnsadmin/pid
User=pdns
Group=pdns
WorkingDirectory=/var/www/html/pdns
ExecStart=/var/www/html/pdns/flask/bin/gunicorn --pid /run/pdnsadmin/pid --bind unix:/run/pdnsadmin/socket 'powerdnsadmin:create_app()'
ExecReload=/bin/kill -s HUP $MAINPID
ExecStop=/bin/kill -s TERM $MAINPID
PrivateTmp=true

[Install]
WantedBy=multi-user.target
EOL
cat > /etc/systemd/system/pdnsadmin.socket << 'EOL'
[Unit]
Description=PowerDNS-Admin socket

[Socket]
ListenStream=/run/pdnsadmin/socket

[Install]
WantedBy=sockets.target
EOL
echo "d /run/pdnsadmin 0755 pdns pdns -" >> /etc/tmpfiles.d/pdnsadmin.conf &&
mkdir /run/pdnsadmin/ &&
chown -R pdns: /run/pdnsadmin/ &&
chown -R pdns: /var/www/html/pdns/powerdnsadmin/ &&
systemctl enable --now pdnsadmin.service pdnsadmin.socket &&
systemctl status pdnsadmin.service pdnsadmin.socket &&
sudo ufw allow "Nginx Full" &&

success_write "Installation de PowerDNS réussie" || fail_write "Une erreur est survenue"
}
#Exécution Manuelle
# cd /var/www/html/pdns/ && source ./flask/bin/activate && ./run.py


#789



function run_serve()
{
  cd /var/www/html/pdns/ && source ./flask/bin/activate && ./run.py
}

<<COMMENT


                                            
COMMENT

"$@"
